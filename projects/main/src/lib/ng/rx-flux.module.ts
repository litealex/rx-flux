import { NgModule } from '@angular/core';
import { StateManagerService } from './rx-flux.service';

@NgModule({
	providers: [StateManagerService]
})
export class RxFluxModule { }