import { Observable } from 'rxjs';

export type Partial<T> = {
	[P in keyof T]?: T[P] | Observable<T[P]> | Promise<T[P]>;
};
