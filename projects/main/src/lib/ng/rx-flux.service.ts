import { Injectable } from '@angular/core';
import { StateManager } from '../core';

@Injectable()
export class StateManagerService<TState> extends StateManager<TState> {

}