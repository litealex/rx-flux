import { Observable, of, from } from 'rxjs';
// import { of } from 'rxjs/observable/of';
// import { fromPromise } from 'rxjs/observable/fromPromise';

function isPromise(obj: any): obj is Promise<any> {
	return !!obj && typeof obj.then === 'function';
}

function isObservable(obj: any): obj is Observable<any> {
	return !!obj && typeof obj.subscribe === 'function';
}

export function wrapIntoObservable<T>(value: T | Observable<T> | Promise<T>):
	Observable<T> {
	if (isObservable(value)) {
		return value;
	}
	if (isPromise(value)) {
		return from(value);
	}

	return of(value);
}
