import { Injectable, Injector } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
// import { combineLatest } from 'rxjs/observable';
// import 'rxjs/add/observable/combineLatest';
// import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/map';

import { StateManagerService } from './rx-flux.service';
import { Partial } from './interfaces';
import { wrapIntoObservable } from './utils';



@Injectable()
export abstract class StateResolver<T extends object> implements Resolve<T> {
  constructor(protected stateManager: StateManagerService<T>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): T | Observable<T> | Promise<T> {
    const nextState = this.nextState(route, state);
    const keys = Object.keys(nextState);

    return combineLatest(
      ...keys.map(x => wrapIntoObservable(nextState[x]))
    ).pipe(
      map((obs: any[]) => keys.reduce((v, k, i) => {
        v[k] = obs[i];
        return v;
      }, {}) as T),
      tap(x => {
        this.stateManager.update((s, d) => {
          return Object.assign({}, s, d);
        }, x);
      })
    )
    // return Observable
    // 	.combineLatest(
    // 	...keys.map(x => wrapIntoObservable(nextState[x]))
    // 	)
    // 	.map((obs:any[]) => keys.reduce((v, k, i) => {
    // 		v[k] = obs[i];
    // 		return v;
    // 	}, {}) as T)
    // 	.do(x => {
    // 		this.stateManager.update((s, d) => {
    // 			return Object.assign({}, s, d);
    // 		}, x);
    // 	});
  }

  abstract nextState(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Partial<T>;




}


