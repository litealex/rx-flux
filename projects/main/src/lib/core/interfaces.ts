import { Observable } from 'rxjs';
export interface IUpdateStateFn<TState, TDelta> {
  (state: TState, delta: TDelta): TState
}

export interface IStatePropertyAccessor<TState, TMap> {
  (state: TState): TMap
}


export interface ICheckFn<TState> {
  (state: TState): boolean;
}

export interface IStateManager<TState> {
  listen<TMap>(accessor?: IStatePropertyAccessor<TState, TMap>): Observable<TMap>;
  value(): TState;
  update<TDelta>(updateFn: IUpdateStateFn<TState, TDelta>, delta: TDelta): TState;
  initState(state: TState): TState;
  chain(): ISafeChain<TState>;
}


export interface ISafeChain<TState> {
  safeGet<K extends keyof TState>(name: K): ISafeChain<TState[K]>
  safeSet(value: TState): void;
  asObservable(): Observable<TState>
  value(): TState
}

