import { Observable, BehaviorSubject } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/distinctUntilChanged'

import {
  IStateManager,
  IStatePropertyAccessor,
  IUpdateStateFn,
  ICheckFn,
  ISafeChain
} from './interfaces';

export class StateManager<TState> implements IStateManager<TState> {

  private readonly defaultEmptyCheck = (x: any) => x === null || x === undefined;
  private state: TState;
  private state$: BehaviorSubject<TState> = new BehaviorSubject<TState>(null);


  listen<TMap>(listenFilter?: IStatePropertyAccessor<TState, TMap>): Observable<TMap> {
    if (listenFilter === undefined) {
      return this.state$
        .pipe(
          map(x => (x as any) as TMap)
        )
    }
    return this.state$
      .pipe(
        distinctUntilChanged(),
        map(x => listenFilter(x)),
        distinctUntilChanged()
      );

  }

  value(): TState {
    return this.state;
  }

  update<TDelta>(updateFn: IUpdateStateFn<TState, TDelta>, delta: TDelta): TState {
    const nextState = updateFn(this.state, delta);
    this.next(nextState);
    return nextState;
  }
  initState(state: TState): TState {
    this.next(state);
    return state;
  }

  chain(): ISafeChain<TState> {
    return new SafeChain(this);
  }
  private next(state: TState): void {
    this.state = state;
    setTimeout(() => this.state$.next(this.state));
  }


}

export class SafeChain<TStateBase, TState> implements ISafeChain<TState> {



  constructor(private baseStateManager: IStateManager<TStateBase>, private chains: string[] = []) {

  }
  safeGet<K extends keyof TState>(name: K): ISafeChain<TState[K]> {
    return new SafeChain<TStateBase, TState[K]>(this.baseStateManager, this.chains.concat(name as any)); // ?
  }

  safeSet(value: TState): void {
    this.baseStateManager.update((s, d) => {
      return this.safeSetter(this.baseStateManager.value(), d, this.chains);
    }, value);
  }

  // todo: add type safety
  private safeSetter(baseState: any, delta: any, chains: string[]): any {
    if (chains.length == 0) {
      return delta;
    }
    const key = chains[0];
    return {
      ...baseState,
      [key]: this.safeSetter(baseState[key] || {}, delta, chains.slice(1))
    }
  }

  asObservable(): Observable<TState> {
    return this.baseStateManager.listen()
      .pipe(
        map(x => this.safeGetter(x, this.chains)),
        distinctUntilChanged()
      );



  }
  value(): TState {
    return this.safeGetter(this.baseStateManager.value(), this.chains);
  }

  private safeGetter(obj: any, chains: string[]): TState {
    let tempObj: any = obj;
    for (let i = 0; i < chains.length; i++) {
      if (tempObj === null || tempObj == undefined) {
        return tempObj;
      }
      tempObj = tempObj[chains[i]];
    }

    return tempObj;
  }
}
