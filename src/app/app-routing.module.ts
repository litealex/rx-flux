import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataComponent } from './data/data.component';
import { DataResolver } from './data/data.resolver';

const routes: Routes = [{
  path: 'data',
  component: DataComponent,
  resolve: {
    state: DataResolver
  }
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
