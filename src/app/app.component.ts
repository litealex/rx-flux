import { Component, OnInit } from '@angular/core';
import { StateManagerService } from 'projects/main/src/public_api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  a = this.state.listen(x => x.title)
  title = this.state.chain().safeGet('title').asObservable();
  constructor(private state: StateManagerService<{ title?: string }>) {

  }

  ngOnInit() {
    this.state.initState({});
    setTimeout(() => {
      this.state.chain().safeGet('title').safeSet('Hello');
      this.state.update((prevState, delta) => {
        return {
          ...prevState,
          title: delta
        };
      }, "Hello");
    }, 2000);
  }
}
