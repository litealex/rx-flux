import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { RxFluxModule } from 'projects/main/src/public_api';
import { DataComponent } from './data/data.component';
import { DataResolver } from './data/data.resolver';
@NgModule({
  declarations: [
    AppComponent,
    DataComponent
  ],
  imports: [
    BrowserModule,
    RxFluxModule,
    AppRoutingModule
  ],
  providers: [DataResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
