import { Injectable } from '@angular/core';

import {
  Route,
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { StateResolver, StateManagerService } from 'projects/main/src/public_api';
import { of } from 'rxjs';


@Injectable()
export class DataResolver extends StateResolver<any> {
  constructor(protected stateManager: StateManagerService<any>) {
    super(stateManager)
  }

  nextState(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): { [x: string]: any; } {
    return {
      text: 'Hello',
      name: of('Alexander')
    }
  }
}
