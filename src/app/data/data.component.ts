import { Component, OnInit } from '@angular/core';
import { StateManagerService } from 'projects/main/src/public_api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  text$:Observable<string> = this.state.chain().safeGet('text').asObservable();
  name$:Observable<string>  = this.state.chain().safeGet('name').asObservable();
  constructor(private state: StateManagerService<any>) { }

  ngOnInit() {
  }

}
