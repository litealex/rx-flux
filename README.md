# rx-flux
rx-flux is state manager inspired by flux

[![npm version](https://badge.fury.io/js/rx-flux.svg)](https://badge.fury.io/js/rx-flux)

## Features
- One immutable state
- Simple action for state updateting
- Support Angular routing resolvers 
- Access for store by obervables

## Instalation 


```
npm install -S rx-flux
```

## Getting started
### Step 1: Import RxFluxModule

``` typescript
import { RxFluxModule } from 'rx-flux';

@NgModule({
...
  imports: [
    ...,
    RxFluxModule
  ]
...
})
export class AppModule { }
```

### Step 2: Describe apllication state interface
``` typescript
export interface IApplicationState {
  title?: string
}
```

### Step 3: Import StateManagerService and init state

``` typescript
import { StateManagerService } from 'rx-flux';

...
constructor(private state: StateManagerService<IApplicationState>) {
}
...
ngOnInit() {
  this.state.initState({});
}
...
```

### Step 4: Subscribe on state
``` typescript
export class AppComponent {
  title$ = this.state.listen(x => x.title);
}

// or

export class AppComponent {
  title$ = this.state.chain().safeGet('title').asObservable();
}
```

### Step 5: Update state
``` typescript
this.state.update((prevState, delta) => {
        return {
          ...prevState,
          title: delta
        };
      }, 'Hello');

// or

this.state.chain().safeGet('title').safeSet('Hello');
```



## Resolver example
``` typescript
export interface IAboutState {
	text: string;
	name: string;
}
```

``` typescript
@Injectable()
export class AboutResolverService extends StateResolver<IAboutState> {
  constructor(protected stateManager: StateManagerService<IAboutState>) {
    super(stateManager);
  }
  nextState(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Partial<IAboutState> {
    return {
      text: 'Helo',
      name: of('Alexander')
    }
  }
}

```

``` typescript
RouterModule.forChild([{
	path: '',
	component: AboutComponent,
	resolve: {
		state: AboutResolverService
	}
}])
```


``` typescript
export class AboutComponent implements OnInit {

  
  constructor(private state: StateManagerService<IAboutState>) { 

  }
  text$:Observable<string> = this.state.chain().safeGet('text').asObservable();
  name$:Observable<string>  = this.state.chain().safeGet('name').asObservable();

  ngOnInit() {
  }

}
```
